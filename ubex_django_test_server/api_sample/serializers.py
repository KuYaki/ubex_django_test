from rest_framework import serializers
from .models import Sample


class SampleSerializer(serializers.Serializer):
	title = serializers.CharField(max_length=120)
	description = serializers.CharField()
	body = serializers.CharField()

	def create(self, validated_data):
		return Sample.objects.create(**validated_data)

	def update(self, instance, validated_data):
		instance.title = validated_data.get('title', instance.title)
		instance.description = validated_data.get('description', instance.description)
		instance.body = validated_data.get('body', instance.body)
		instance.save()
		return instance
