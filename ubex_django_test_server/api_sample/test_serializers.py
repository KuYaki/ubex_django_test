from django.test import TestCase
from .serializers import SampleSerializer
from .models import Sample


class SampleSerializerTestCase(TestCase):
	def setUp(self):
		Sample.objects.create(
			title="title 1",
			description="desc 1",
			body="bla bla bla 1")
		Sample.objects.create(
			title="title 2",
			description="desc 2",
			body="bla bla bla 2")

	def test_sample_serializer_on_one_sample(self):
		sample = Sample.objects.get(title="title 1")
		serializer = SampleSerializer(sample, many=True)
		self.assertEqual(serializer.data, '{title="title1",description="desc 1",body="bla bla bla 1"}')
