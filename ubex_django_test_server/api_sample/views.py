from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Sample
from .serializers import SampleSerializer
import json


class ApiSampleView(APIView):
	def get(self, request):
		limit = request.GET.get("LIMIT", None)
		order_by = request.GET.get("ORDER", None)
		filters_json = request.GET.get("FILTER", None)
		if filters_json is None:
			samples = Sample.objects.all()
		else:
			args = json.loads(filters_json)
			samples = Sample.objects.filter(**args)
		if order_by is not None:
			samples = samples.order_by(order_by)
		if limit is not None:
			samples = samples[:int(limit)]

		serializer = SampleSerializer(samples, many=True)
		return Response({"samples": serializer.data})

	def post(self, request):
		sample = request.data.get('sample')
		serializer = SampleSerializer(data=sample)
		if serializer.is_valid(raise_exception=True):
			sample_saved = serializer.save()
			return Response({"success": "Sample '{}' created successfully".format(sample_saved.title)})

	def put(self, request, pk):
		saved_sample = get_object_or_404(Sample.objects.all(), pk=pk)
		data = request.data.get('sample')
		serializer = SampleSerializer(instance=saved_sample, data=data, partial=True)
		if serializer.is_valid(raise_exception=True):
			sample_saved = serializer.save()
			return Response({
				"success": "Sample '{}' updated successfully".format(sample_saved.title)
			})

	def delete(self, request, pk):
		sample = get_object_or_404(Sample.objects.all(), pk=pk)
		sample.delete()
		return Response({
			"message": "Sample with id `{}` has been deleted.".format(pk)
		}, status=204)
