from django.db import models


class Sample(models.Model):
	objects = models.Manager()
	title = models.CharField(max_length=120)
	description = models.TextField()
	body = models.TextField()

	def __str__(self):
		return self.title
