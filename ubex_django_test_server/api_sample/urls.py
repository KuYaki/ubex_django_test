from django.urls import path
from .views import ApiSampleView

app_name = "api_sample"
urlpatterns = [
	path('sample/', ApiSampleView.as_view()),
	path('sample/<int:pk>', ApiSampleView.as_view()),
]
