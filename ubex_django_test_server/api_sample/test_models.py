from django.test import TestCase
from .models import Sample


class SampleTestCase(TestCase):
	def setUp(self):
		Sample.objects.create(
			title="title 1",
			description="desc 1",
			body="bla bla bla 1")
		Sample.objects.create(
			title="title 2",
			description="desc 2",
			body="bla bla bla 2")

	def test_samples_description(self):
		sample1 = Sample.objects.get(title="title 1")
		sample2 = Sample.objects.get(title="title 2")
		self.assertEqual(sample1.description, "desc 1")
		self.assertEqual(sample2.description, "desc 2")

	def test_samples_body(self):
		sample1 = Sample.objects.get(title="title 1")
		sample2 = Sample.objects.get(title="title 2")
		self.assertEqual(sample1.body, "bla bla bla 2")
		self.assertEqual(sample2.body, "bla bla bla 2")
