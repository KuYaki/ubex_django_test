# UBEX Django Test

## Зависимости

Для корректной работы требуется python3.5+

Установите все зависимости, выполнив следующую команду из корневой папки проекта:

```
pip install -r requirements.txt
```

## Адрес сервера

```
http://127.0.0.1:8000/api
```

## Запуск

Перейдите в директорию ubex_django_test_server (содержащую файл manage.py)
и выполните:

```
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```

## Взаимодействие

Проверить апи можно с помощью следующих запросов:

###GET
```
http://127.0.0.1:8000/api/sample/
http://127.0.0.1:8000/api/sample/?FILTER={"title":"title 2","description":"desc 2"}
http://127.0.0.1:8000/api/sample/?ORDER=title&LIMIT=5
```

###POST
```
http://127.0.0.1:8000/api/sample/
```
body:
```
{
  "sample": {
    "title": "title 1",
    "description": "desc 1",
    "body": "bla bla bal 1"
  }
}
```

###PUT
```
http://127.0.0.1:8000/api/sample/1
```
body:
```
{
  "sample": {
    "title": "title 2",
    "description": "desc 2",
    "body": "bla bla bal 2"
  }
}
```

###DELETE
```
http://127.0.0.1:8000/api/sample/1
```

## Тестирование

Перейдите в директорию ubex_django_test_server (содержащую файл manage.py)
и выполните:

```
python manage.py test
```
